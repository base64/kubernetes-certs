# kubernetes证书

#### 介绍
kubeadm 在创建群集时，将生成证书过期完全封装了，ca证书有效期为10年，其它证书有效时为1年 ，这里手动生成k8s证书

/etc/kubernetes/pki  
/etc/kubernetes/pki/etcd

22 个证书文件   
```conf
|-- apiserver.crt
|-- apiserver-etcd-client.crt
|-- apiserver-etcd-client.key
|-- apiserver.key
|-- apiserver-kubelet-client.crt
|-- apiserver-kubelet-client.key
|-- ca.crt
|-- ca.key
|-- etcd
|   |-- ca.crt
|   |-- ca.key
|   |-- healthcheck-client.crt
|   |-- healthcheck-client.key
|   |-- peer.crt
|   |-- peer.key
|   |-- server.crt
|   `-- server.key
|-- front-proxy-ca.crt
|-- front-proxy-ca.key
|-- front-proxy-client.crt
|-- front-proxy-client.key
|-- sa.key
|-- sa.pub

